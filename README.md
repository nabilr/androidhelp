**
Documentation repository for android.**


* [How to install apktool](http://www.itechnospot.com/blog/howtos/how-to-install-and-use-apktool-in-ubuntu-14-04/)
* [How to use apktool]( http://ibotpeaches.github.io/Apktool/documentation/)


**Android Reverse Engineering** 

* [ddx2dot](https://github.com/nelhage/reverse-android/blob/master/ddx2dot)
* [Reflection basics](http://code.tutsplus.com/tutorials/learn-java-for-android-development-reflection-basics--mobile-3203)
* [Java byte code fundamentals](http://arhipov.blogspot.com/2011/01/java-bytecode-fundamentals.html)
* [Rverse engineering 1](http://www.slideshare.net/PranayAiran1/reverse-engineering-android-apps)

**Android Internals**

* [Android binder](https://www.youtube.com/watch?v=LBqpXaivRmY&list=PLp8TnZfz-r1761W5cksOx6488Io7F-Eky)

* [Android Internals Overview](https://www.youtube.com/watch?v=MlxiQNijniQ)

http://www.slideshare.net/paller/understanding-the-dalvik-bytecode-with-the-dedexer-tool

http://www.slideshare.net/jserv/understanding-the-dalvik-virtual-machine

http://www.slideshare.net/ssusere3af56/how-to-implement-a-simple-dalvik-virtual-machine

http://www.slideshare.net/jserv/practice-of-android-reverse-engineering

http://www.slideshare.net/SrinivasKothuri/dalvik-jit-31830080

http://www.slideshare.net/jserv/vm-construct

http://www.slideshare.net/ThomasRichards/dancing-with-dalvik

http://www.slideshare.net/jserv/low-level-view-of-android-system-architecture

http://www.slideshare.net/paller/dedexer

http://www.slideshare.net/AndrewDFIR/blackhat-workshopfullpresentation

http://www.slideshare.net/limaniBhavik/artaot-vs-dalvikjit

http://www.slideshare.net/ssusere3af56/how-to-implement-a-simple-dalvik-virtual-machine?next_slideshow=1